from flask import render_template, redirect, url_for
from fimnexus import app, lm, cp
from fimnexus.models import Usr
from fimnexus.forms import HomepageSearchForm, NexusQueryFiltersForm

@lm.user_loader
def get_user_by_id(id):
    return Usr.query.get(int(id))

@app.route('/', methods=['GET', 'POST'])
def home():
    form = HomepageSearchForm()
    if form.validate_on_submit():
        # 从数据库中查询有无记录
        # 若有则跳转到详情页面nexus_detail，或因页面未通过审核返回未审核的提示信息
        # 若无则跳转到新建节点页面nexus_create
        return form.q.data
    return render_template('home.html', form=form, newest=[], most_wanted=[])

@cp.exempt
@app.route('/nexus/all', methods=['GET', 'POST'])
def nexus_query():
    form = NexusQueryFiltersForm()
    if form.validate_on_submit():
        return render_template('nexus_query.html', records=[], form=form)
    return render_template('nexus_query.html', records=[], form=form)

@app.route('/nexus/new', methods=['GET', 'POST'])
def nexus_create():
    return render_template('nexus_create.html')

@app.route('/nexus/<int:id>', methods=['GET', 'POST'])
def nexus_detail(id):
    return render_template('nexus_detail.html')

@app.route('/te')
def test():
    return render_template('test.html')