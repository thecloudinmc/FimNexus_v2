from flask_wtf import FlaskForm
from wtforms.fields import StringField, SelectField
from wtforms.validators import DataRequired, ValidationError

class HomepageSearchForm(FlaskForm):
    q = StringField(validators=[DataRequired(message='地址不能为空')])

    def validate_q(self, q):
        if 'www.fimfiction.net/story/' not in q.data:
            raise ValidationError('请输入有效地址 ∑( 口 ||')

class NexusQueryFiltersForm(FlaskForm):
    sort = SelectField('排序方式', choices=[('longest','长到短'),
                                            ('shortest','短到长'),
                                            ('most_wanted','最多人请求'),
                                            ('newest','新到旧')])
    length = SelectField('篇幅', choices=[('short','短篇（一万以下）'),
                                            ('medium','中篇（一万到五万）'),
                                            ('long','长篇（五万以上）')])
    state = SelectField('小说进度', choices=[(-1,'作者弃坑'),
                                            (0,'暂停'),
                                            (1,'连载中'),
                                            (2,'已完结')], coerce=int)