from datetime import datetime, timedelta
from flask_login import UserMixin
from fimnexus import db

aso_usr_fic = db.Table('aso_usr_fic',
    db.Column('usr_id', db.Integer, db.ForeignKey('usr.id'), primary_key=True),
    db.Column('fic_id', db.Integer, db.ForeignKey('fic.id'), primary_key=True)
)

class Usr(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)

    username = db.Column(db.String(16), unique=True)
    password = db.Column(db.String(60))
    level = db.Column(db.Integer, default=0)

    fics = db.relationship('Fic', secondary=aso_usr_fic, back_populates='usrs')

    def __init__(self, username, password):
        self.username = username
        self.password = password

class Fic(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(128))
    wcount = db.Column(db.Integer)
    author = db.Column(db.String(64))
    state = db.Column(db.Integer) # -1：已取消，0：已暂停，1：正在连载，2：已完结
    url = db.Column(db.Text)
    wanted = db.Column(db.Integer, default=0)
    create_at = db.Column(db.DateTime, default=datetime.utcnow()+timedelta(hours=8))

    usrs = db.relationship('Usr', secondary=aso_usr_fic, back_populates='fics')
    cmts = db.relationship('Cmt', back_populates='fic', cascade="all, delete, delete-orphan")

    def __init__(self, url):
        self.url = url

class Cmt(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    cmter = db.Column(db.String(16))
    ctn = db.Column(db.Text)
    create_at = db.Column(db.DateTime, default=datetime.utcnow()+timedelta(hours=8))
    is_top = db.Column(db.Boolean, default=False)

    fic_id = db.Column(db.Integer, db.ForeignKey('fic.id'))
    fic = db.relationship('Fic', back_populates='cmts')

    def __init__(self, cmter, ctn):
        self.cmter = cmter
        self.ctn = ctn